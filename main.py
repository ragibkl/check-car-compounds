import sys
import urllib3
import requests

from bs4 import BeautifulSoup
from requests.exceptions import RequestException
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


def retry(func):
    def wrapper(*args, retries=5, exc=None, **kwargs):
        if retries < 1:
            raise exc

        try:
            return func(*args, **kwargs)
        except RequestException as e:
            retries = retries - 1
            message = (
                f'execution failed for func: {func.__name__}, '
                f'args: {args}, retries_left: {retries}, '
                f'exception: {e}'
            )
            print(message)
            return wrapper(*args, retries=retries, exc=e, **kwargs)

    return wrapper


@retry
def get_mbpj_data(plate_number):
    url = 'http://eps.mbpj.gov.my/splk/kmplisting.asp'
    data = {
        'optSearchType': 'nodaftar',
        'inpsearchvalue': plate_number,
        'submit': 'Cari',
    }

    s = requests.Session()
    s.get(url)
    r = s.post(url, data=data)

    if 'TAHNIAH!' in r.text:
        return False
    return True


@retry
def get_mpsepang_data(plate_number):
    url = 'http://ekl.mpsepang.gov.my/eklapi/api/services/getflexicompaundbyvehicleno'
    params = {
        'vehicleno': plate_number
    }

    s = requests.Session()
    r = s.get(url, params=params)

    if r.json()['OK'] is False:
        return False
    return True


@retry
def get_mpsj_data(plate_number):
    get_url = 'https://enquiry.mpsj.gov.my/v2/service/kom_search/'
    post_url = 'https://enquiry.mpsj.gov.my/v2/service/kom_search_submit/'
    view_url = f'{get_url}1'

    s = requests.Session()
    r_get = s.get(get_url)

    soup = BeautifulSoup(r_get.text, features='html.parser')
    access_key_tag = soup.find('form', id='ciform') \
        .find('input', type='hidden')

    data = {
        'sel_input': 'no_kereta',
        'sel_value': plate_number,
        'ACCESS_KEY': access_key_tag['value'],
    }
    s.post(post_url, data=data)
    r = s.get(view_url)

    if 'Tiada data' in r.text:
        return False
    return True


@retry
def get_mbsa_data(plate_number):
    get_url = 'https://eps.mbsa.gov.my/'
    post_url = 'https://eps.mbsa.gov.my/public/payment/semak'

    s = requests.Session()
    r_get = s.get(get_url)

    soup = BeautifulSoup(r_get.text, features='html.parser')
    token_tags = soup.find('form', id='kompaunSearchForm') \
        .find_all('input', type='hidden')
    tokens = [t['value'] for t in token_tags]

    data = {
        '_token': tokens,
        'selServ': 1,
        'servId': plate_number,
        'datedari': '2018-01-01',
        'dateke': '2020-01-01',
    }
    r = s.post(post_url, data=data)
    result = r.json()

    if result['count'] == 0:
        return False
    return True


@retry
def get_dbkl_data(plate_number, ic):
    get_url = 'https://appsys.dbkl.gov.my/ezbill/index.asp'
    post_url = 'https://appsys.dbkl.gov.my/ezbill/sen_kompauntrafik_f_2.asp'

    s = requests.Session()
    s.get(get_url, verify=False)

    data = {
        'noplat': plate_number,
        'nokp': ic,
        'Submit': 'Proses',
    }
    r = s.post(post_url, data=data, verify=False)

    if 'TIADA SAMAN DIREKODKAN' in r.text:
        return False

    soup = BeautifulSoup(r.text, features='html.parser')
    rows = soup.form.table.contents
    rows = [r for r in rows if r != '\n']
    rows = rows[3:-2]

    count = 0
    for r in rows:
        table = r.contents[-4].table
        div = table.contents[1].find_all('div')[-1]
        val = div.contents[0].strip()

        if val not in ['X', 'B']:
            count = count + 1

    return count > 0


@retry
def get_mpkajang_data(plate_number):
    url = 'https://ebayar.mpkj.gov.my/webapi/api/services/getflexicompaundbyvehicleno'
    params = {
        'vehicleno': plate_number
    }

    s = requests.Session()
    r = s.get(url, params=params)

    res = r.json()
    if not res or len(res) == 1 and res[0]['Amount'] == 0:
        return False
    return True


@retry
def get_mpklang_data(plate_number):
    url = 'http://iportal.mpklang.gov.my/kompaun21.cfm'
    data = {
        'nokenderaan': plate_number,
        'button': 'Submit',
    }

    s = requests.Session()
    r = s.post(url, data=data)

    if 'Tiada kompaun bagi kenderaan' in r.text:
        return False
    return True


@retry
def get_mps_data(plate_number):
    url = 'http://public.mps.gov.my/ekompaunmpsweb/kmplisting.asp'
    data = {
        'optSearchType': 'nodaftar',
        'inpsearchvalue': plate_number,
        'B1': 'Cari',
    }

    s = requests.Session()
    r = s.post(url, data=data)
    res = r.text

    if 'Tiada rekod kompaun' in res or 'tiada rekod kompaun' in res:
        return False
    return True


def check_car_compounds(plate_number, ic=None):
    return {
        'plate_number': plate_number,
        'ic': ic,
        'mbpj': get_mbpj_data(plate_number),
        'mpsepang': get_mpsepang_data(plate_number),
        'mpsj': get_mpsj_data(plate_number),
        'mbsa': get_mbsa_data(plate_number),
        'dbkl': get_dbkl_data(plate_number, ic),
        'mpkajang': get_mpkajang_data(plate_number),
        'mpklang': get_mpklang_data(plate_number),
        'mps': get_mps_data(plate_number),
    }


def read_input_file():
    cars = []
    with open('input.csv', 'r', encoding='utf-8') as f:
        for line in f.readlines()[1:]:
            elems = line.split(',')
            car = {
                'plate_number': elems[0].strip(),
                'ic': elems[1].strip(),
            }
            cars.append(car)
    return cars


def write_output_file(results):
    headers = [
        'plate_number',
        'ic',
        'mbpj',
        'mpsepang',
        'mpsj',
        'mbsa',
        'dbkl',
        'mpkajang',
        'mpklang',
        'mps',
    ]
    header = ','.join(headers) + '\n'
    with open('output.csv', 'w', encoding='utf-8') as f:
        f.write(header)
        for r in results:
            data = [
                r['plate_number'],
                r['ic'],
                r['mbpj'],
                r['mpsepang'],
                r['mpsj'],
                r['mbsa'],
                r['dbkl'],
                r['mpkajang'],
                r['mpklang'],
                r['mps'],
            ]
            data = [str(d) for d in data]
            line = ','.join(data) + '\n'
            f.write(line)


def main():
    inputs = read_input_file()

    results = []
    for i in inputs:
        plate_number = i['plate_number']
        ic = i['ic']

        try:
            result = check_car_compounds(plate_number, ic)
            results.append(result)
            print(result)
        except KeyboardInterrupt:
            print('KeyboardInterrupt')
            break
        except Exception as e:
            print(e)
            print(f'failed request. plate_number: {plate_number}, id: {ic}')
            results.append({
                'plate_number': plate_number,
                'ic': ic,
                'mbpj': None,
                'mpsepang': None,
                'mpsj': None,
                'mbsa': None,
                'dbkl': None,
                'mpkajang': None,
                'mpklang': None,
                'mps': None,
            })

    write_output_file(results)


if __name__ == '__main__':
    main()
